{{/*
Expand the name of the chart.
*/}}
{{- define "AppCtx.name" -}}
{{- default .Chart.Name | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Expand the name of the api.
*/}}
{{- define "AppCtx.apiName" -}}
{{- $apiName := default .Release.Name | trunc 63 | trimSuffix "-" }}
{{- printf "%s-api" $apiName }}
{{- end }}

{{/*
Expand the name of the front.
*/}}
{{- define "AppCtx.frontName" -}}
{{- $frontName := default .Release.Name | trunc 63 | trimSuffix "-" }}
{{- printf "%s-front" $frontName }}
{{- end }}

{{/*
Expand the name of the db.
*/}}
{{- define "AppCtx.dbName" -}}
{{- $dbName := default .Release.Name | trunc 63 | trimSuffix "-" }}
{{- printf "%s-db" $dbName }}
{{- end }}

{{/*
Application image tag
We select by default the Chart appVersion or an override in values
*/}}
{{- define "AppCtx.imageTag" }}
{{- $name := default .Chart.AppVersion .Values.image.tag }}
{{- printf "%s" $name }}
{{- end }}


{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
*/}}
{{- define "AppCtx.fullname" }}
{{- $name := default .Chart.Name .Values.nameOverride -}}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-"}}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "AppCtx.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "AppCtx.labels" -}}
helm.sh/chart: {{ include "AppCtx.chart" . }}
{{ include "AppCtx.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "AppCtx.selectorLabels" -}}
app.kubernetes.io/name: {{ include "AppCtx.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}
